<?php
$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/bender/dre_art2sales/';
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = [
	'id'          => 'dre_art2sales',
	'title'       => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="Bodynova Admin Suche Modul">odynova Artikel vom Endkundenshop',
	'description' => [
		'de' => 'Globale Suche im Bodynova Shop-Backend',
		'en' => 'Global search shop admin'
	],
	'version'     => '2.0.0',
    'thumbnail'     => 'out/img/logo_bodynova.png',
	'author'      => 'André Bender',
	'url'         => '',
	'extend'      => [
	    /*
		\OxidEsales\Eshop\Application\Controller\Admin\NavigationController::class
        => \Bender\dre_AdminSearch\Controller\Admin\NavigationController::class,
	    */
	],
	'controllers' => [],
	'templates'   => [],
	'blocks'      => [
		[
		    /*
			'template' => 'navigation.tpl',
			'block' => 'admin_navigation_menustructure',
			'file' => '/views/blocks/admin_navigation_menustructure.tpl'
		    */
		],
	],
	'settings'    => [
	    /*
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowArticles',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowCategories',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowCmsPages',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowOrders',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowUsers',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowVendors',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowManufacturers',
			'type' => 'bool',
			'value' => true
		],
		[
			'group' => 'oxcom_adminsearch_main',
			'name' => 'blOxComAdminSearchShowModules',
			'type' => 'bool',
			'value' => true
		],
	    */
	],
];
